#include "keymap_common.h"

const uint8_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  KEYMAP(F1,  F2,   ESC,  1, 2,   3,   4,   5,   6,   7,   8,   9,   0,   MINS,EQL, BSPC, GRV, NLCK, SLCK,\
         F3,  F4,   TAB,   Q, W, E,   R,   T,   Y,   U,   I,   O,   P,   LBRC,RBRC, P7, P8, P9, PMNS ,    \
         F5,  F6, FN0, A,   S,   D,   F,   G,   H,   J,   K,   L,   SCLN,QUOT,ENT,P4,  P5, P6, PPLS,          \
         F7,  F8, LSFT, Z,   X,   C,   V,   B,   N,   M,   COMM,DOT, SLSH,RSFT,  BSLS, P1, P2, P3, PENT,         \
         F9, F10, LCTL, LALT,           SPC,                     RGUI,P0,PDOT),

   /* 1: FN 1 */
   KEYMAP(F11,  F12,   TRNS,  TRNS, TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,TRNS, TRNS, TRNS, TRNS, TRNS,\
          TRNS,  TRNS,   TRNS,TRNS,   TRNS,  TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,TRNS, TRNS, TRNS, TRNS, TRNS ,    \
          TRNS,  TRNS, TRNS, TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,  TRNS, TRNS,TRNS,TRNS,  TRNS, TRNS, TRNS,          \
          TRNS,  TRNS, TRNS, TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,   TRNS,TRNS, TRNS,TRNS,  TRNS, TRNS, TRNS, TRNS, TRNS,         \
          TRNS, TRNS, TRNS, TRNS,           TRNS,                     TRNS,TRNS,TRNS),
};

const action_t PROGMEM fn_actions[] = {
  [0] = ACTION_LAYER_MOMENTARY(1)
};
